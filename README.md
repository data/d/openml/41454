# OpenML dataset: dragons

https://www.openml.org/d/41454

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Citing information: https://modeloriented.github.io/DALEX2/authors.html

Dataset dragons is artificially generated. Values are generated in a way to:
- have nonlinearity in year_of_birth and height
- have concept drift in the test set
Attributes:
- year_of_birth - year in which the dragon was born. Negative year means year BC, eg: -1200 = 1201 BC
- year_of_discovery - year in which the dragon was found
- height - height of the dragon in yards
- weight - weight of the dragon in tons
- scars - number of scars
- colour - colour of the dragon
- number_of_lost_teeth - number of teeth that the dragon lost
- life_length - life length of the dragon

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41454) of an [OpenML dataset](https://www.openml.org/d/41454). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41454/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41454/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41454/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

